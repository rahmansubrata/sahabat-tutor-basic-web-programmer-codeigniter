<?= $this->extend('template/V_template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-4">
    <div class="card">
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Nama : <?= $mahasiswa->nama ?></li>
                <li class="list-group-item">NIM : <?= $mahasiswa->nim ?></li>
                <li class="list-group-item">Alamat : <?= $mahasiswa->alamat ?></li>
                <li class="list-group-item">Jurusan : <?= $mahasiswa->jurusan ?></li>

                <li class="list-group-item"><a href="<?= base_url('ubahdata/' . $mahasiswa->id); ?>" class="btn btn-success my-3">Update</a></li>
                <form action="<?= base_url() ?>/mahasiswa/delete_mahasiswa/<?= $mahasiswa->id; ?>" method="post" class="mb-3 list-group-item ">
                    <?= csrf_field(); ?>
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class=" btn btn-danger" onclick="return confirm('apakah anda yakin?');">Delete</button>
                </form>
                <li class="list-group-item">
                    <a href="<?= base_url('mahasiswa') ?>" class="btn btn-primary">Halaman Utama</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>