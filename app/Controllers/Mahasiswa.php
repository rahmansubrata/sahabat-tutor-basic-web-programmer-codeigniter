<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use \App\Models\MahasiswaModel;

class Mahasiswa extends BaseController
{
    protected $MahasiswaModel;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('mahasiswa');
        $this->MahasiswaModel = new MahasiswaModel();
    }

    public function index()
    {
        $mahasiswa = $this->MahasiswaModel->findAll();
        $data['mahasiswa'] = $mahasiswa;
        return view('V_mahasiswa', $data);
    }
    public function detail($id)
    {
        $this->builder = $this->db->table('mahasiswa');
        $this->builder->where('id', $id);
        $query = $this->builder->get();
        $data['mahasiswa'] = $query->getRow();
        return view('V_detail_mahasiswa', $data);
    }
    public function delete_mahasiswa($id)
    {
        //menghapus tanaman budidaya
        $this->MahasiswaModel->delete($id);
        session()->setFlashdata('pesan_hapus', 'Data berhasil Dihapus');
        return redirect()->to('/mahasiswa');
    }
    public function form_tambah()
    {

        // dd($data['penyakit']);
        return view('V_form_tambah');
    }
    public function form_update($id)
    {
        $penyakit = $this->MahasiswaModel->getmahasiswa($id);
        $data['mahasiswa'] = $penyakit->getRow();
        // dd($data['penyakit']);
        return view('V_form', $data);
    }
    public function prosestambah()
    {
        $this->MahasiswaModel->save([
            'nama' => $this->request->getVar('nama'),
            'nim' => $this->request->getVar('nim'),
            'alamat' => $this->request->getVar('alamat'),
            'jurusan' => $this->request->getVar('jurusan')
        ]);
        session()->setFlashdata('pesan_berhasil', 'Data berhasil Diubah');
        return redirect()->to('/mahasiswa');
    }
    public function updatemahasiswa($id)
    {
        $this->MahasiswaModel->save([
            'id' => $id,
            'nama' => $this->request->getVar('nama'),
            'nim' => $this->request->getVar('nim'),
            'alamat' => $this->request->getVar('alamat'),
            'jurusan' => $this->request->getVar('jurusan')
        ]);
        session()->setFlashdata('pesan_berhasil', 'Data berhasil Diubah');
        return redirect()->to('/mahasiswa');
    }
}
