<?= $this->extend('template/V_template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="btn-group my-3">
        <a href="<?= base_url() ?>/form_tambah" class="btn btn-primary active" aria-current="page">Tambah Data</a>
    </div>
    <?php if (session()->getFlashdata('pesan_berhasil')) : ?>
        <div class="alert alert-success mt-3" role="alert">
            <?= session()->getFlashdata('pesan_berhasil') ?>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashdata('pesan_hapus')) : ?>
        <div class="alert alert-danger mt-3" role="alert">
            <?= session()->getFlashdata('pesan_hapus') ?>
        </div>
    <?php endif; ?>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Jurusan</th>
                    <th scope="col">Tindakan</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <?php foreach ($mahasiswa as $mhs) : ?>
                    <tr>
                        <th scope="row"><?= $i++; ?></th>
                        <td><?= $mhs['nama']; ?></td>
                        <td><?= $mhs['nim']; ?></td>
                        <td><?= $mhs['alamat']; ?></td>
                        <td><?= $mhs['jurusan']; ?></td>
                        <td>
                            <a href="<?= base_url('detailuser/' . $mhs['id']); ?>" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?= $this->endSection('content'); ?>