<?= $this->extend('template/V_template'); ?>

<?= $this->section('content'); ?>
<div class="container mt-4">
    <form method="post" action="<?= base_url() ?>/prosestambah">
        <div class="mb-3">
            <label for="formGroupExampleInput" class="form-label">Nama</label>
            <input name="nama" type="text" class="form-control" id="formGroupExampleInput" placeholder="Masukan nama lengkap">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput2" class="form-label">NIM</label>
            <input name="nim" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukan NIM">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput3" class="form-label">Jurusan</label>
            <input name="jurusan" type="text" class="form-control" id="formGroupExampleInput3" placeholder="contoh : Teknik Pertanian dan Biosistem">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput4" class="form-label">Alamat</label>
            <input name="alamat" type="text" class="form-control" id="formGroupExampleInput4" placeholder="Alamat Lengkap">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<?= $this->endSection('content'); ?>